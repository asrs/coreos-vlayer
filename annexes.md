
-- CoreOS : Documentation
https://docs.fedoraproject.org/en-US/fedora-coreos/

-- Coreos : rpm-ostree FS
https://www.youtube.com/watch?v=KpUkUhOvqbw
https://developers.redhat.com/blog/2020/03/12/how-to-customize-fedora-coreos-for-dedicated-workloads-with-ostree#rebasing

-- Customize CoreOs
https://coreos.github.io/coreos-installer/customizing-install/
https://cloud.redhat.com/blog/red-hat-enterprise-linux-coreos-customization

-- CoreOS Layering Update / customization
https://github.com/coreos/layering-examples

-- Prometheus exporter for libvirt
https://github.com/Tinkoff/libvirt-exporter
https://github.com/zhangjianweibj/prometheus-libvirt-exporter

-- CoreOs libvirt spice password issue
https://askubuntu.com/questions/780862/virt-manager-to-remote-host-continuous-password-prompt-over-ssh

-- CoreOs terraform libvirt provider
https://registry.terraform.io/providers/dmacvicar/libvirt/latest
https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs # DOCUMENTATION YEAH

-- CoreOs Assembler
https://coreos.github.io/coreos-assembler/

-- CoreOs debug guide
https://www.digitalocean.com/community/tutorials/how-to-troubleshoot-common-issues-with-your-coreos-servers
https://man.archlinux.org/man/ostree.1.en
https://github.com/ostreedev/ostree/issues/1793https://github.com/ostreedev/ostree/issues/1793

-- CoreOs from RAM
https://docs.fedoraproject.org/en-US/fedora-coreos/live-booting/

-- CoreOS / Silverblue PCI passthrough
https://github.com/FilBot3/fedora-silverblue-gpu-passthrough

-- CoreOs Libvirt prometheus exporter via containers
https://hub.docker.com/r/alekseizakharov/libvirt-exporter

