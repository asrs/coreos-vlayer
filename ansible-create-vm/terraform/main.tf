terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.1"
    }
  }
}
variable "diskBytes" { default = 1024*1024*1024*20 }

resource "libvirt_volume" "bats-disk" {
  name   = "bats-disk"
  format = "qcow2"
  size = var.diskBytes
}

resource "libvirt_domain" "bats" {

  name = "bats"
  description = "the bats vm"
  autostart = true
  vcpu = 2
  memory = 1024

  boot_device {
    dev = [ "hd","cdrom", "network"]
  }

  network_interface {
    network_name = "default"
  }

  disk {
    volume_id = libvirt_volume.bats-disk.id
  }

  disk {
    file = "/var/lib/libvirt/images/alpine-virt-3.17.2-x86_64.iso"
  }

 console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
    listen_address = "0.0.0.0"
  }
}



