-------------------------------------------------------------------------
# Create a Vlayer with CoreOS and Libvirt

### Why :
Solution as openstack got a big learning curve and low automation for d01 operations
Solution as Proxmox doesn't have d01 automation features, and automation is lacking 
of flexibility.

Also the underlying OS is subject to security issue and maintenance a regular OS

### CoreOS :
CoreOS provide an immutable rock solid Operating System with a very low attack
surface.

It's also easily reproducible, and can be automate on Day01 operations with tools
such as Tinkerbell

CoreOS configuration rely on a JSON file call Ignition file.
This file can be written in YAML and then compile to JSON via a tool call Butane

### Libvirt :
Libvirt provide a full API for automation.

Tools such as Terraform and Ansible can easily manage the creation / deletion of
virtual machine.

Libvirt give a templating XML file that can be modified to manage VM

Hashicorp Packer can create Golden Image to libvirt

-------------------------------------------------------------------------
## Prepare installation on Libvirt machine

- By default Root user in not accessible
- An user call Core is present
- Core doesn't have SSH key nor Password
- coreos-install command let us inject an ignition file in the ISO

we need to create a ignition file for the installation such as :
```
variant: fcos
version: 1.4.0
passwd:
  users:
    - name: core
      password_hash: $y$j9T$xkdXuTMM/y/mxyIlGk2Px/$NBA2L5qMfLQCA2IrrV9t4RcgrNqd3AEpr2m.ixVqosB
	- name: admin
      password_hash: $y$j9T$xkdXuTMM/y/mxyIlGk2Px/$NBA2L5qMfLQCA2IrrV9t4RcgrNqd3AEpr2m.ixVqosB
      groups:
        - "sudo"
```

you can generate a password hash with the following command :
```
podman run -ti --rm quay.io/coreos/mkpasswd --method=yescrypt
```

see the documentation : https://docs.fedoraproject.org/en-US/fedora-coreos/authentication/

once the YAML is written we need to run Butane to generate the ignition file :
```
podman run --interactive --rm quay.io/coreos/butane:release \
       --pretty --strict < your_butane_config.yaml > my-ignition.ign
```

You can expose from a host the generated file as such :
```
podman run --rm -p 8080 -v ${PWD}:/usr/share/nginx/html:z nginx
```

You can now retrieve the file on the virt machine :
```
wget 192.168.122.1:8080/my-ignition.ign
```

-------------------------------------------------------------------------
## install CoreOs on the libvirt machine

you can run the installation like this :
```
sudo coreos-installer install /dev/sda \
    --insecure-ignition \
    --ignition-url 192.168.122.1:8080/my-ignition.ign
```

then you can connect with the user and password to the os.

note by default the ssh connection by password is not activated and should
be parameter in ignition file

-------------------------------------------------------------------------
## Update ignition Post-install

to update a machine configuration one installed the process is the same

we write a new ignition file :
```
variant: fcos
version: 1.4.0
passwd:
  users:
    - name: core
      password_hash: $y$j9T$xkdXuTMM/y/mxyIlGk2Px/$NBA2L5qMfLQCA2IrrV9t4RcgrNqd3AEpr2m.ixVqosB

    - name: admin
      password_hash: $y$j9T$xkdXuTMM/y/mxyIlGk2Px/$NBA2L5qMfLQCA2IrrV9t4RcgrNqd3AEpr2m.ixVqosB
      groups:
        - sudo
storage:
  files:
    - path: /etc/hostname
      mode: 0644
      contents:
        inline: |
          vlayer.hoplab
    - path: /etc/ssh/sshd_config.d/20-enable-passwords.conf
      mode: 0644
      contents:
      inline: |
        # Enable SSH password login
        PasswordAuthentication yes
```

You need to re-install the machine to change the Ignition file as this file is 
mean to run only once



-------------------------------------------------------------------------
## Libvirt Issue with DNS resolution

first update the metadata
```
rpm-ostree refresh-md
```

if it failed please change /etc/resolv.conf, on libvirt there is dns 
configuration issue :
```
nameserver 8.8.8.8
```

you could edit the file permanently with nmcli
```
nmcli con edit <connection name> set ipv4.dns 8.8.8.8
```


-------------------------------------------------------------------------
## installing package with rpm-ostree

first update the metadata
```
rpm-ostree refresh-md
```

then you can install your package by using this command :
```
rpm-ostree install -A qemu libvirt
```

-------------------------------------------------------------------------
## layering modification and modification for CoreOS

some example : https://github.com/coreos/layering-examples

create a working directory to build container

create a container file based on the release of coreos you want, and with
the modification you want too :
```
#ContainerFile
FROM quay.io/fedora/fedora-coreos:stable

RUN rpm-ostree install qemu libvirt vim git rsyslog

RUN rm /etc/motd && \
	echo "hello my layering" > /etc/motd

ADD remote.conf /etc/rsyslog.d/remote.conf
```

build the image :
```
podman build -t $IMAGE_NAME -f /path/to/Containerfile
```

push it to a repository :
```
podman push localhost/my-custom-fcos quay.io/<YOURUSER>/my-custom-fcos
```

Then connect to your CoreOS and stop the update service to avoid collision with rebase action :
```
systemctl stop zincati
```

rebase from a remote registry:
```
rpm-ostree rebase ostree-unverified-registry:registry/user/container:tag
```


-------------------------------------------------------------------------
## Diff analyze of CoreOS

# /!\ SHOULD NOT RESTORECONTEXT ON COREOS /!\

the following command show the difference on the tree :
```
ostree admin config-diff
```

erase modification or add :
```
rsync -avh /usr/etc/yourfile /etc/yourfile --delete
```

-------------------------------------------------------------------------
## Passthrough note

rpm-ostree kargs \
  --append-if-missing="intel_iommu=on" \
  --append-if-missing="iommu=pt" \
  --append-if-missing="rd.driver.pre=vfio_pci" \

rpm-ostree initramfs \
  --enable \
  --arg="--add-drivers" \
  --arg="vfio-pci" \
  --reboot

sudo rpm-ostree kargs \
  --append-if-missing="vfio-pci.ids=8086:15f2" \
  --reboot

rpm-ostree install bridge-utils


-------------------------------------------------------------------------
## CoreOS run from RAM

download the ISO: 
```
podman run --security-opt label=disable --pull=always --rm -v .:/data -w /data \
    quay.io/coreos/coreos-installer:release download -f iso
```

customise the ISO like this:
```
podman run --security-opt label=disable --pull=always --rm -v .:/data -w /data \
    quay.io/coreos/coreos-installer:release iso customize --live-ignition config.ign \
    -o customized.iso fedora-coreos-37.20230218.3.0-live.x86_64.iso
```


-------------------------------------------------------------------------
## Note boot from local iso

create /etc/live (via ignition)


inject entry in /etc/grub.d/40_custom: (via ignition)
```
menuentry "Alpine Live"{
    set isofile="/etc/live/alpine-virt-3.17.2-x86_64.iso"
    search --no-floppy -f --set=root $isofile
    probe -u $root --set=abc
    set pqr="/dev/disk/by-uuid/$abc"
    loopback loop $isofile
    linux  (loop)/boot/vmlinuz-x86_64  img_dev=$pqr img_loop=$isofile copytoram
    initrd  (loop)/boot/intel_ucode.img (loop)/boot/initramfs-x86_64.img
}
```

copy iso on the /etc/live (via ansible)

run the grub2-mkconfig


to relaunch the ignition
```
unshare -m /bin/sh -c 'mount -o remount,rw /boot && touch /boot/ignition.firstboot'
```



-------------------------------------------------------------------------
## install terraform on fedora 37

install the repo :
```
sudo dnf install -y dnf-plugins-core
sudo dnf config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo
```

install terraform :
```
sudo dnf install -y terraform
```

-------------------------------------------------------------------------
## basic terraform 

create a working directory :
```
mkdir -p terraform
cd terraform
```
create a main.tf :
```
touch main.tf
vim ./main.tf
```

add the provider in main.tf :
```
terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.1"
    }
  }
}

provider "libvirt" {
  # Configuration options
}
```

you can now run the init to check if the provider can be download :
```
terraform init
```

you can create an env var to manage the qemu connection :
```
export LIBVIRT_DEFAULT_URI="qemu+ssh://root:redhat@192.168.1.100/system"
```

then use :
```
terraform init
terraform plan
terraform apply
terraform destroy
```

-------------------------------------------------------------------------

## Deploy Prometheus and grafana with Helm

check the connection to the cluster
```
export KUBECONFIG=path/to/your/config
kubectl get node
```

install helm :
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

add the stable repository :
```
helm repo add stable https://charts.helm.sh/stable
```

launch the installation of the operator :
```
helm install stable/prometheus-operator --generate-name
```

-----------

On the GUEST run your exporter as container :
```
podman run --privileged -p9177:9177 -v /var/run/libvirt:/var/run/libvirt alekseizakharov/libvirt-exporter:latest
```

then add on the HOST prometheus configuration the end point :
```
# > /etc/prometheus/prometheus.yaml

    static_configs:
      - targets: ["localhost:9090"]
      - targets: ["192.168.100.15:9177"]
```

