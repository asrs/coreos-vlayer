#!/bin/sh
# Usage: launch.sh -c '/path/bats-config.yml' -k '/path/to/kubeconfig'

helpFunction()
{
   echo ""
   echo "Usage: $0 -c '/path/to/node-config.yml'"
   exit 1 # Exit script after printing help
}

while getopts "c:" opt
do
   case "$opt" in
      c ) nodeconfig="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$nodeconfig" ]
then
   echo "missing parameters";
   helpFunction
fi

# Begin script in case all parameters are correct
echo "The node config file path is :: $nodeconfig"
echo ""
echo "generating the ignition"
echo ""


ansible-playbook \
--extra-vars "@$nodeconfig" \
./main.yml
